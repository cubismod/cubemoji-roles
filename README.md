# Cubemoji Roles

JSON schema and definition files used for managing roles within cubemoji.

# Node
You need Node.js installed to use [the shell script](test.sh)


# What does this do?

This is a role management feature for Discord. You can define roles using flexible JSON which will allow [Cubemoji](https://gitlab.com/com/cubismod/cubemoji) to generate webpages from. Discord users can request an ephemeral link to setup their roles.

# How do I contribute?
1. Make a fork.
2. Use the [schema](./schema/roles.json) and reference files in [data](./data/) to create a datafile.
3. Create a merge request with the upstream repo.
4. A quick [CI script](./.gitlab-ci.yml) which validates the data against the schema.
5. The change may or may not be accepted. If accepted, it will deploy to the bot in production.